+++
title = "Sketchbook"
date =  2021-04-09T12:50:05+01:00
weight = 5
+++

##### Flipbooks in sketchbook seem to be quite simple to work with, 

first off you need to plan how long you want your animation to be  
then apply the frame rate to it, so a 5 sec animation at 24 fps will be 120 frames

##### Keep in mind keyboard shortcuts: 

`,` is to go back a frame and `.` will go forward one frame

##### Layers:  
Layers for flip books work diffrently to how regular layers work, you only have 2 working layers and the rest will be static
![layers](./layers.png)

##### Onion skins:
A really useful tool to use is onionskinning which will display the last and next frame which you can use to blend frames and check positions
![onion](./onion.png)