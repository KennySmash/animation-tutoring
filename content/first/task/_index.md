+++
title = "Task"
date =  2021-04-09T12:52:18+01:00
weight = 5
+++

#### Your Task for your first assignment is to animate one of your gram pictures, this one :  
![Your art](./art-to-animate.jpg?width=20pc&classes=shadow)  

##### there are a few requirements for the animated version:  
| Task | Description |
| ------ | ----------- |
| background   | you need to do some cleanup on the linework and backkground color/texture to seperate out the subject from the background  |
| symbols | the symbols need to do something that sparks life into them, I suggest a glow effect that fades in and out and maybe look at subtly chaging the sillouette |
| glasses    | animate in a light sweep over them, I've provided an example [here](../animation) in the Animation page |
| mouth pattern    | animate the pattern flowing out the mouth and disapearing in some way, try to do it without it falling off the bottom of the page |